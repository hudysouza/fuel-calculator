import 'package:flutter/cupertino.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

import 'input.widget.dart';
import 'loading-button.widget.dart';

class SubmitForm extends StatelessWidget {
  final MoneyMaskedTextController gasCtrl;
  final MoneyMaskedTextController alcCtrl;
  final bool busy;
  final Function submitFunction;

  const SubmitForm(
      {@required this.gasCtrl,
      @required this.alcCtrl,
      @required this.busy,
      @required this.submitFunction});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          child: Input(
            ctrl: gasCtrl,
            label: "Gasolina",
          ),
          padding: EdgeInsets.only(left: 30, right: 30),
        ),
        Padding(
          child: Input(
            ctrl: alcCtrl,
            label: "Álcool",
          ),
          padding: EdgeInsets.only(left: 30, right: 30),
        ),
        LoadingButton(
          busy: busy,
          func: submitFunction,
          invert: false,
          text: "CALCULAR",
        )
      ],
    );
  }
}
